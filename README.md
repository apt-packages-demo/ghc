# ghc

[**ghc**](https://tracker.debian.org/pkg/ghc) The Glorious Glasgow Haskell Compilation system (GHC) is a compiler for Haskell.

* Debian package includes a lot of libraries.
* Libraries in separates packages can depend on those distributed in the ghc Debian package.
* Libraries packages and ghc package sould preferably be from the same Debian version.
* In Debian experimental, only the ghc Debian package may be available. In this case, Debian unstable Haskell library packages may not be installed simultaneously with ghc experimental, because of version conflict with the compiler itself or the libraries distributed in the same Debian package.
